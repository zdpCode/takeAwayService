package com.example.waimai;

import com.example.waimai.serviceImp.OrdersServiceImp;
import com.example.waimai.util.SensitiveUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

@SpringBootTest
class WaimaiApplicationTests {

    @Autowired
    private OrdersServiceImp ordersServiceImp1;
    @Autowired
    private OrdersServiceImp ordersServiceImp2;

    @Test
    void contextLoads() {
    }

    @Test
    void test1() throws IOException {
            ClassPathResource classPathResource = new ClassPathResource("static/myBadWordsDemo.txt");
            BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(classPathResource.getFile()),"UTF-8"));
            String str = null;
            Set<String> sets = new HashSet<>();
            while((str = in.readLine()) != null){
                sets.add(str);
            }
            Iterator<String> iterator = sets.iterator();
            Map nowMap = null;
            Map<String, String> newWorMap = null;
            HashMap sensitiveWordMap = new HashMap(sets.size());
            while(iterator.hasNext()){
                    String key = iterator.next();
                    nowMap = sensitiveWordMap;
                for(int i = 0 ; i < key.length() ; i++){
                        char keyChar = key.charAt(i);       //转换成char型
                        Object wordMap = nowMap.get(keyChar);       //获取

                        if(wordMap != null){        //如果存在该key，直接赋值
                            nowMap = (Map) wordMap;
                        }
                        else{     //不存在则，则构建一个map，同时将isEnd设置为0，因为他不是最后一个
                            newWorMap = new HashMap<String,String>();
                            newWorMap.put("isEnd", "0");     //不是最后一个
                            nowMap.put(keyChar, newWorMap);
                            nowMap = newWorMap;
                        }

                        if(i == key.length() - 1){
                            nowMap.put("isEnd", "1");    //最后一个
                        }
                    }
                }
        System.out.println("xxx");
    }


    @Autowired
    private SensitiveUtil sensitiveUtil;
    // 测试敏感词
    @Test
    public void testSensitive(){
        String str = "我是一个中国 人中国 男人";
        String sensitive = sensitiveUtil.findSensitive(str);
        System.out.println(sensitive);
    }




}
