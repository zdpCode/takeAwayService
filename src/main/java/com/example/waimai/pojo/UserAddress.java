package com.example.waimai.pojo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class UserAddress  {
    private Integer id;
    private String receiveName;
    private Integer userId;
    private String telephone;
    private String address;
    private String bottom;
    private String longitude;
    private String latitude;
    private Integer status;
}
