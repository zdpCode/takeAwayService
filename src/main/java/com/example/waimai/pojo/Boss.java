package com.example.waimai.pojo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Boss {
    private Integer id;
    private String telephone;
    private String password;
    private String bossName;
    private String image;
    private String email;
    private Float money;
}
