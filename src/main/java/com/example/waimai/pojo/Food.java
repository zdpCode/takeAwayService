package com.example.waimai.pojo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Food {
    private Integer id;
    private String foodName;
    private String foodImage;
    private Float foodPrice;
    private String foodInfo;
    private Integer storeId;
    private Integer foodStatus;
    private Integer foodStock;
    private Integer foodTypeId;

    // 额外属性 ： 添加了多少菜
    private Integer addNums = 0;
    // 记录一下菜单类型名
    private String foodTypeName;
}
