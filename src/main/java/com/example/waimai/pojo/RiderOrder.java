package com.example.waimai.pojo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RiderOrder {
    // 针对 骑手端这边要显示的信息的类
    private Orders order;
    // 用户的地址信息
    private UserAddress userAddress;
    // 店铺的地址信息
    private StoreAddress storeAddress;
    // 店铺名
    private String storeName;

}
