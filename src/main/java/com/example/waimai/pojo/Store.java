package com.example.waimai.pojo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigInteger;

@Getter
@Setter
@ToString
public class Store {
    private Integer id;
    private String storeName;
    private String storeImage;
    private String storeType;
    private BigInteger storeNo;
    private Integer bossId;
    private String storeAddress;
    private Integer storeStatus; // 0 : 未审核 1 : 审核通过 2 :审核未通过
    private String storeInfo;
}
