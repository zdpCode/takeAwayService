package com.example.waimai.pojo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
public class FoodType {
    private Integer id;
    private String foodTypeName;
    private Integer storeId;

    private List<Food> subFoods = new ArrayList<>(); // 该类别的菜集合
}
