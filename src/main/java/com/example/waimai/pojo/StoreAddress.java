package com.example.waimai.pojo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class StoreAddress {
    private Integer id;
    private Integer storeId;
    private String address;
    private String bottome;
    private String longitude;
    private String latitude;
}
