package com.example.waimai.pojo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class User {
    private Integer id;
    private String telephone;
    private String password;
    private String userName;
    private String email;
    private String userImage;
}
