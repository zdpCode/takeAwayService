package com.example.waimai.pojo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.ibatis.annotations.Select;

@Getter
@Setter
@ToString
public class Comment {
    private Integer id;
    private Integer storeLevel;
    private Integer riderLevel;
    private Integer orderId;
    private String storeContent;
    private String riderContent;
}
