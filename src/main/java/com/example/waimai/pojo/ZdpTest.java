package com.example.waimai.pojo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class ZdpTest {
    private Integer id;
    private String name;
    private String content;
}
