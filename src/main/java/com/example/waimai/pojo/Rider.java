package com.example.waimai.pojo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Rider {
    private Integer id;
    private String telephone;
    private String password;
    private String riderName;
    private String email;
    private String image;
    private Float money;
}
