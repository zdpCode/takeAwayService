package com.example.waimai.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.sql.Timestamp;
import java.util.List;

@Getter
@Setter
@ToString
public class Orders {
    private Integer id;
    private Integer userAddressId;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    private Timestamp startTime;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    private Timestamp endTime;
    private Float deliveryFee;
    private Float totalPrice;
    private String remark;
    private Integer userStatus;
    private Integer storeStatus;
    private Integer riderStatus;
    private Integer userId;
    private Integer storeId;
    private Integer riderId;
    private String foodIdList; // 也要记录 数量
    private List<Food> _foodList; // 将菜品 集合
    private UserAddress userAddress;
    private StoreAddress storeAddress;
    private Store store;
}
