package com.example.waimai.test;

import java.io.*;
import java.util.*;

public class impDemo implements demoInterface{
    @Override
    public void demoTest() {
        System.out.println("demo1");
    }

    @Override
    public void common() {
        System.out.println("comm1");
    }

    public static void main(String[] args) {
        List<String> obj = new ArrayList<>();
        StringBuilder s = new StringBuilder();
        StringBuffer ss = new StringBuffer();
        String sss = new String();
    }

/*
    public class NIOServer extends Thread {
        public void run() {
            try (Selector selector = Selector.open();
                 ServerSocketChannel serverSocket = ServerSocketChannel.open();) {// 创建Selector和Channel
                serverSocket.bind(new InetSocketAddress(InetAddress.getLocalHost(), 8888));
                serverSocket.configureBlocking(false);
                // 注册到Selector，并说明关注点
                serverSocket.register(selector, SelectionKey.OP_ACCEPT);
                while (true) {
                    selector.select();// 阻塞等待就绪的Channel，这是关键点之一
                    Set<SelectionKey> selectedKeys = selector.selectedKeys();
                    Iterator<SelectionKey> iter = selectedKeys.iterator();
                    while (iter.hasNext()) {
                        SelectionKey key = iter.next();
                        // 生产系统中一般会额外进行就绪状态检查
                        sayHelloWorld((ServerSocketChannel) key.channel());
                        iter.remove();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        private void sayHelloWorld(ServerSocketChannel server) throws IOException {
            try (SocketChannel client = server.accept();) {          client.write(Charset.defaultCharset().encode("Hello world!"));
            }
        }
        // 省略了与前面类似的main
    }*/

//测试一下敏感词
    public void mgc() throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream("static/myBadWords.txt"),"UTF-8"));
        String str = null;
        Set<String> sets = new HashSet<>();
        while((str = in.readLine()) != null){
            sets.add(str);
        }
        Iterator<String> iterator = sets.iterator();
        while(iterator.hasNext()){
            String ss = iterator.next();
            System.out.println(ss);
        }
    }
}
