package com.example.waimai.context;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "spring.redis")
@Component
@Setter
@Getter
@ToString
public class JedisProperties {

    private String host = "127.0.0.1";
    private String port = "6379";
    private String password ="";
}
