package com.example.waimai.context;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class ResultMsg<T> {

    private int code;
    private String message;
    private T data;

    public ResultMsg(T data){
        this(ResultCode.SUCCESS,data);
    }

    public ResultMsg(ResultCode resultCode, T data){
        code = resultCode.getCode();
        message = resultCode.getMsg();
        this.data = data;
    }

}
