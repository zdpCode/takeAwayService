package com.example.waimai.context;

public class JwtUserContext {

    //使用一个 ThreadLocal 来存储信息，防止多线程冲突
    //Thredlocal:本地线程变量
    /*
     * Threadlocal 为变量在每个线程都创建了一个副本，那么多个线程曹祖这个变量就不会产生冲突
     * */
    private static final ThreadLocal<String> userTelephone = new ThreadLocal<>();

    public static void add(String userName){
        JwtUserContext.userTelephone.set(userName);
    }
    public static void remove(){
        userTelephone.remove();
    }
    //获得当前请求的用户名
    public static String getCurrentName(){
        return userTelephone.get();
    }
}
