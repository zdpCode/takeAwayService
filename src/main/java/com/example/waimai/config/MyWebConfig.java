package com.example.waimai.config;

import com.example.waimai.handler.JwtHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MyWebConfig  implements WebMvcConfigurer {
    //注册拦截器
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        InterceptorRegistration interceptorRegistration = registry.addInterceptor(new JwtHandler());
        //可以通过 InterceptorRegistration 来配置拦截的路径等待 和不拦截的路径
       /* interceptorRegistration.addPathPatterns();
        interceptorRegistration.excludePathPatterns();*/
    }
}
