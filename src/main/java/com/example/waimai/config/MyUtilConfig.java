package com.example.waimai.config;

import com.example.waimai.util.SensitiveUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MyUtilConfig {

    @Bean
    public SensitiveUtil sensitiveUtil(){
        SensitiveUtil sensitiveUtil = new SensitiveUtil();
        sensitiveUtil.initSensitiveWords("myBadWords.txt");
        sensitiveUtil.initSensitiveMap();
        return sensitiveUtil;
    }

}
