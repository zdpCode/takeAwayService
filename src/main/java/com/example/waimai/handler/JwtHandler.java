package com.example.waimai.handler;

import com.example.waimai.context.JwtUserContext;
import com.example.waimai.util.JwtUtil;
import io.jsonwebtoken.Claims;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class JwtHandler implements HandlerInterceptor {

    /*
    * 请求处理前 拦截
    *  true: 放行
    *  false :  错误
    * */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // 放行登录请求
        String url = request.getRequestURI();
        /*if("UserLogin".equals(url)){
            return true;
        }
        String authorization = request.getHeader("Authorization");
        Claims parse = JwtUtil.parse(authorization);
        if(null == parse){
            return false;
        }
        JwtUserContext.add(parse.getSubject());*/
        return true;
    }
}
