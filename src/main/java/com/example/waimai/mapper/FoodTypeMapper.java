package com.example.waimai.mapper;

import com.example.waimai.pojo.FoodType;
import org.apache.ibatis.annotations.Insert;

import java.util.List;

public interface FoodTypeMapper {

    public List<FoodType> getFoodTypesByStoreId(Integer id);

    Integer insertFoodType(FoodType foodType);
}
