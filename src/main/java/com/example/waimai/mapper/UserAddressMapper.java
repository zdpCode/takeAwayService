package com.example.waimai.mapper;

import com.example.waimai.pojo.UserAddress;

import java.util.List;

public interface UserAddressMapper {

     int insertIntoUserAddress(UserAddress userAddress);

     List<UserAddress> getAllByUserId(Integer id);

     UserAddress getAddressByUserAddressId(Integer id);

     Integer updateUserAddress(UserAddress userAddress);

}
