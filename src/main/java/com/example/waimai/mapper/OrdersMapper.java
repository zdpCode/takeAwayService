package com.example.waimai.mapper;

import com.example.waimai.pojo.Orders;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface OrdersMapper {

    // 生成一条新订单
    int insertNewOrder(Orders order);

    Orders getOrderById(Integer orderid);

    // 更新订单信息
    int updateOrder(Orders order);

    // 获取到骑手 根据状态 的订单
    List<Orders> getOrdersByIdAndStatus(@Param("riderId") Integer riderId, @Param("riderStatus") Integer riderStatus);

    // 根据商家状态来获取订单
    @Select("select * from orders where storeId = #{storeId} AND storeStatus = #{storeStatus} AND userStatus != -1")
    List<Orders> getOrdersByStoreStatus(@Param("storeId")Integer storeId,@Param("storeStatus")Integer storeStatus);

    // 获取用户所有订单
    @Select("select * from orders where userId = #{id} ORDER BY id DESC ")
    List<Orders> getOrdersByUserId(Integer userId);

    // 获取骑手的所有订单
    @Select("select * from orders where riderId = #{id} ORDER BY riderStatus DESC")
    List<Orders> getOrdersByRiderId(Integer riderId);

    @Select("select * from orders where riderId = #{riderId} AND userStatus >= #{userStatus}")
    List<Orders> getOrdersByRiderIdAndUserStauts(@Param("riderId")Integer riderId,@Param("userStatus")Integer userStatus);

    @Select("select * from orders where storeId in (select storeId from store where bossId = #{bossId}) AND userStatus >= #{userStatus}")
    List<Orders> getOrdersByBossIdAndUserStatus(@Param("bossId")Integer bossId,@Param("userStatus")Integer userStatus);

    @Select("select * from orders where storeId = #{storeId} " +
            "AND (storeStatus = #{storeStatus} OR userStatus = -1 OR storeStatus = -1 )")
    List<Orders> getEndOrders(@Param("storeId")Integer storeId,@Param("storeStatus")Integer storeStatus);
}
