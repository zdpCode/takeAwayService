package com.example.waimai.mapper;

import com.example.waimai.pojo.Food;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface FoodMapper {

    // 获取到该店铺的菜品,并按 foodType id值排序
    public List<Food> getFoodsByStoreId(Integer storeId);

    public List<Food> getFoods();

    // 删除菜品
    @Delete("DELETE FROM food WHERE id = #{foodId}")
    Integer delFood(Integer foodId);

    // 删除菜品，应该是更新状态
    Integer updateFood(Food food);

    // 添加菜品
    Integer addFood(Food food);

    // 查找菜品
    @Select("select * from food where id = #{foodId}")
    Food getFood(Integer foodId);

}
