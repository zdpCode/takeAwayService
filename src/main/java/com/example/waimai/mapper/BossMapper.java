package com.example.waimai.mapper;

import com.example.waimai.pojo.Boss;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Service;

public interface BossMapper {

    @Select("select * from boss where telephone = #{tel} AND password = #{pwd}")
    Boss selectBytelAndPwd(@Param("tel")String tel,@Param("pwd")String pwd);

    Integer insertBoss(Boss boss);

    Integer updateBoss(Boss boss);

    @Select("select * from boss where id = #{id}")
    Boss selectBossById(Integer id);

}
