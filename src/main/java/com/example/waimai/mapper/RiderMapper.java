package com.example.waimai.mapper;

import com.example.waimai.pojo.Rider;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface RiderMapper {

    @Select("select * from rider where telephone = #{telephone} AND password = #{password}")
    Rider getRdierByTelAndPwd(@Param("telephone")String telephone,@Param("password")String password);

    Integer insertRider(Rider rider);

    Integer updateRider(Rider rider);

    @Delete("delete from rider where id = #{riderId}")
    Integer delRdier(Integer riderId);

    @Select("select * from rider where id = #{id}")
    Rider getRiderById(Integer id);

}
