package com.example.waimai.mapper;

import com.example.waimai.pojo.User;
import org.apache.ibatis.annotations.Param;

public interface UserMapper {

    //
    public User UserLogin(@Param("telephone") String telephone, @Param("password") String password);

    Integer insertUser(User user);

    Integer updateUser(User user);

}
