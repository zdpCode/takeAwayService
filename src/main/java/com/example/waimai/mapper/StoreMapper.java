package com.example.waimai.mapper;

import com.example.waimai.pojo.Store;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Service;

import java.util.List;

public interface StoreMapper {

    @Select("select * from store")
    public List<Store> getStores();

    @Select("select * from store where id = #{id}")
    public Store getStore(Integer id);

    // 更新店铺，不仅仅是图片
    Integer updateStore(Store store);

    // 获取到所有店铺
    @Select("select * from store where bossId = #{id} AND storeStatus >=1")
    List<Store> getAllStore(Integer id);

    // 添加店铺
    Integer insertStore(Store store);

    // 删除店铺
    @Delete("delete from store where id = #{storeId}")
    Integer delStore(Integer storeId);

    /**
     *
     * @param id 店铺Id
     * @return BossId
     */
    @Select("select bossId from store where id = #{id}")
    Integer getBossIdByStoreId(Integer id);



}
