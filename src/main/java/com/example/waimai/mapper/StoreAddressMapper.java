package com.example.waimai.mapper;

import com.example.waimai.pojo.StoreAddress;

public interface StoreAddressMapper {

    StoreAddress getStoreAddressById(Integer storeId);

    Integer insert(StoreAddress storeAddress);

    Integer update(StoreAddress storeAddress);
}
