package com.example.waimai.mapper;

import com.example.waimai.pojo.Comment;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Service;

public interface CommentMapper {

    Integer insertComment(Comment comment);

    @Select("select * from comment where orderId = #{id}")
    Comment getCommentByOrderId(Integer orderId);
}
