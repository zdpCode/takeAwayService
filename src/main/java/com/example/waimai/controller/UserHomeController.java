package com.example.waimai.controller;

import com.example.waimai.pojo.Food;
import com.example.waimai.pojo.FoodType;
import com.example.waimai.pojo.Store;
import com.example.waimai.serviceImp.StoreServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class UserHomeController {

    @Autowired
    private StoreServiceImp storeServiceImp;

    @GetMapping("LoadUserHome")
    public List<Store> getLoadUserHomeData(){
        return storeServiceImp.getAllStore();
    }

    @PostMapping("loadStoreHome")
    public List<FoodType> getFoodsByStoreId(@RequestBody Integer storeId){
        System.out.println(storeId);

        return storeServiceImp.loadStoreHome(storeId);
    }


    @PostMapping("getFoods")
    public List<Food> getFoods(@RequestBody Integer storeId){

        return storeServiceImp.getFoodsByStoreId(storeId);
    }

    // 获取附近的店铺
    @PostMapping("getMissedStore")
    public List<Store> getMissedStores(@RequestBody Map<String,String> param){
        String longitude = param.get("longitude");
        String latitude = param.get("latitude");
        return storeServiceImp.getMissedStore(longitude,latitude);
    }

}
