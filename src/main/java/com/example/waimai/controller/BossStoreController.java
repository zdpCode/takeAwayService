package com.example.waimai.controller;

import com.example.waimai.pojo.*;
import com.example.waimai.serviceImp.BossStoreServiceImp;
import com.example.waimai.serviceImp.StoreServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

@RestController
public class BossStoreController {

    @Autowired
    private StoreServiceImp storeServiceImp;
    @Autowired
    private BossStoreServiceImp bossStoreServiceImp;

    @PostMapping("getStoreInfo")
    public Store getStoreInfo(@RequestBody Integer storeId){
        return storeServiceImp.getStore(storeId);
    }

    @PostMapping("addFoodType")
    public FoodType addFoodType(@RequestBody FoodType foodType){
        return bossStoreServiceImp.insertFoodType(foodType);
    }

    // 删除指定菜品
    @PostMapping("delFood")
    public Boolean delFood(@RequestBody Food food){
        return bossStoreServiceImp.deleteFood(food);
    }

    // 更新菜品信息
    @PostMapping("updateFood")
    public Boolean updateFood(@RequestBody Food food){
        return bossStoreServiceImp.updateFood(food);
    }

    // 更新店铺图片
    @PostMapping("updateStoreImage")
    public String updateStoreImage(@RequestParam("image")MultipartFile image,
                                   @RequestParam("id") String id){
        return bossStoreServiceImp.updateStoreImage(image,id);
    }

    // 更新菜品图片
    @PostMapping("updateFoodImage")
    public String updateFoodImage(@RequestParam("image")MultipartFile image,
                                  @RequestParam("id") String id){
            return bossStoreServiceImp.updateFoodImage(image,id);
    }

    // 获取到图片的URl地址
    @PostMapping("getImageUrl")
    public String getImageUrl(@RequestParam("image")MultipartFile image){
            return bossStoreServiceImp.getImageUrl(image);
    }
    //添加新的菜品
    @PostMapping("addFood")
    public Integer addFood(@RequestBody Food food){

        return bossStoreServiceImp.addFood(food);
    }

    // 删除店铺
    @PostMapping("delStore")
    public Boolean delStore(@RequestBody Integer storeId){
        // 其实就是修改状态
        return bossStoreServiceImp.delStore(storeId);
    }

    // 获取到所有店铺
    @PostMapping("getAllStore")
    public List<Store> getAllStore(@RequestBody Integer bossId){

        return bossStoreServiceImp.getAllStore(bossId);
    }

    // 获取指定店铺的地址
    @PostMapping("getStoreAddress")
    public StoreAddress getStoreAddress(@RequestBody Integer storeId){

        return bossStoreServiceImp.getStoreAddress(storeId);
    }

    // 根据状态获取订单
    @PostMapping("getOrdersByStoreStatus")
    public List<Orders> getOrdersByStoreStatus(@RequestBody Map<String,Integer> param){
        Integer storeId = param.get("storeId");
        Integer storeStatus = param.get("storeStatus");

        return bossStoreServiceImp.getOrdersByStoreStatus(storeId,storeStatus);
    }

    // 商家接单
    @PostMapping("receiveOrder")
    public Boolean receiveOrder(@RequestBody Integer orderId){

        return bossStoreServiceImp.receiveOrder(orderId);
    }

    // 获取到菜品信息
    @PostMapping("bossgetFood")
    public Food bossGetFood(@RequestBody Integer foodId){
        return bossStoreServiceImp.getFood(foodId);
    }

    // 上传店铺地址信息
    @PostMapping("addStoreAddr")
    public Boolean addStoreAddr(@RequestBody StoreAddress storeAddress){

        return bossStoreServiceImp.addStoreAddr(storeAddress);
    }

    // 创建店铺
    @PostMapping("addStore")
    public Store addStore(@RequestBody Store store){
        store = bossStoreServiceImp.addStore(store);
        return store;
    }

    // 删除掉店铺 （不是改变状态）
    @PostMapping("realDelStore")
    public Boolean realDelStore(@RequestBody Integer storeId){
        return bossStoreServiceImp.realDelStore(storeId);
    }

    // 更新店铺地址
    @PostMapping("updateStoreAddr")
    public Boolean updateStoreAddr(@RequestBody StoreAddress storeAddress){

        return bossStoreServiceImp.updateStoreAddr(storeAddress);
    }
    // 更新店铺信息
    @PostMapping("updateStore")
    public Boolean updateStore(@RequestBody Store store){

        return bossStoreServiceImp.updateStore(store);
    }
}
