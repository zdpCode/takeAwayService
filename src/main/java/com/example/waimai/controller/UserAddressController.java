package com.example.waimai.controller;

import com.example.waimai.pojo.UserAddress;
import com.example.waimai.service.UserAddressService;
import com.example.waimai.serviceImp.UserAddressServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserAddressController {

    @Autowired
    private UserAddressServiceImp userAddressServiceImp;

    @PostMapping(value="addUserAddress" )
    public UserAddress addUserAddress(@RequestBody UserAddress userAddress){
        System.out.println(userAddress.toString());
        UserAddress result = userAddressServiceImp.insertIntoUserAddress(userAddress);
        System.out.println(result.toString());
        return  result;
    }

    @PostMapping(value="getAllUserAddress")
    public List<UserAddress> getAllUserAddress(@RequestBody Integer id){
        List<UserAddress> allByUserId = userAddressServiceImp.getAllByUserId(id);
        return allByUserId;
    }

    // 更新用户地址
    @PostMapping("updateUserAddress")
    public Boolean updateUserAddress(@RequestBody UserAddress userAddress){

        return userAddressServiceImp.updateUserAddress(userAddress);
    }
}
