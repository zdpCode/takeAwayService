package com.example.waimai.controller;

import com.example.waimai.pojo.User;
import com.example.waimai.service.UserUpdateInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class UserUpdateInfoController {


    @Autowired
    private UserUpdateInfoService userUpdateInfoService;

    @PostMapping("updateUser")
    public Boolean updateUser(@RequestBody User user){

        return  userUpdateInfoService.updateUser(user);
    }

    /*更新头像*/
    @PostMapping("updateUserImage")
    public String updateUserImage(@RequestParam("image") MultipartFile image,
                                  @RequestParam("id") String userId){
        return userUpdateInfoService.updateUserImage(image,userId);
    }
}
