package com.example.waimai.controller;

import com.example.waimai.pojo.Boss;
import com.example.waimai.service.BossLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class BossLoginController {

    @Autowired
    private BossLoginService bossLoginService;

    @PostMapping("bossLogin")
    public Boss bossLogin(@RequestBody Map<String,String> param){
        String telephone  = param.get("tel");
        String password = param.get("pwd");
        return bossLoginService.bossLogin(telephone,password);
    }

    @PostMapping("bossRegister")
    public Boss bossRegister(@RequestBody Boss boss){
        return bossLoginService.bossRegister(boss);
    }

    @PostMapping("getBossById")
    public Boss getBossById(@RequestBody Integer id){
        return bossLoginService.getBossById(id);
    }

}
