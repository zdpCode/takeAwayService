package com.example.waimai.controller;

import com.example.waimai.mapper.FoodMapper;
import com.example.waimai.pojo.Food;
import com.example.waimai.pojo.Orders;
import com.example.waimai.pojo.ZdpTest;
import com.example.waimai.serviceImp.MyRabbitService;
import com.example.waimai.serviceImp.StoreServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class demoController {

    @GetMapping("/hello")
    public String Hello(){
        System.out.println("测试hello");
        return "hello";
    }

    @PostMapping("postDemo")
    public ZdpTest postDemo(@RequestBody ZdpTest test){
        System.out.println(test.toString());
        ZdpTest result = new ZdpTest();
        result.setId(2);
        result.setName("郑达平");
        result.setContent("没啥的");
        return result;
    }

    @PostMapping("postDemoArray")
    public List<Map<String,Object>> postDemo2(){
        ArrayList<Map<String,Object>> t = new ArrayList<>();
        ZdpTest result = new ZdpTest();
        result.setId(2);
        result.setName("郑达平");
        result.setContent("没啥的");
        Map<String,Object> tt = new HashMap<>();
        tt.put("郑达平测试",result);
        t.add(tt);
        return t;
    }

    @PostMapping("postDemoArray1")
    public List<ZdpTest> postDemo3(){
        ArrayList<ZdpTest> t = new ArrayList<>();
        ZdpTest result = new ZdpTest();
        result.setId(2);
        result.setName("郑达平");
        result.setContent("没啥的");
        t.add(result);
        return t;
    }

    @Autowired
    private MyRabbitService myRabbitService;

    @PostMapping("demoRabbitMq")
    public void demoRab(){
        Orders orders = new Orders();
        orders.setId(1);
        orders.setRiderId(1);
        orders.setRemark("这是备注");
        myRabbitService.sendOrderToBoss("Bossorders","store:1",orders);
    }

    @Autowired
    private FoodMapper foodMapper;

    @Autowired
    private StoreServiceImp storeServiceImp;

    @PostMapping("demoFoods")
    public List<Food> demoFoods(){
        return foodMapper.getFoods();
    }

    /*@PostMapping("demoFoodMap")
    public HashMap<String,Object> getFoodMap(@RequestBody Integer storeId){

        return storeServiceImp.getFoodMap(storeId);
    }*/
}
