package com.example.waimai.controller;

import com.example.waimai.pojo.Comment;
import com.example.waimai.service.CommentService;
import com.example.waimai.serviceImp.CommentServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CommentController {

    @Autowired
    private CommentServiceImp commentServiceImp;

    @PostMapping("getCommentByOrderId")
    public Comment getCommnetByOrderId(@RequestBody Integer orderId){
        return commentServiceImp.getCommentByOrderId(orderId);
    }
}
