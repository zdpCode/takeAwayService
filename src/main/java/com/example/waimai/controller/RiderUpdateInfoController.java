package com.example.waimai.controller;

import com.example.waimai.pojo.Rider;
import com.example.waimai.service.RiderUpdateInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class RiderUpdateInfoController {
    @Autowired
    private RiderUpdateInfoService riderUpdateInfoService;

    @PostMapping("updateRiderImage")
    public String updateRiderImage(@RequestParam("image") MultipartFile image,
                                   @RequestParam("id") String riderId){

        return riderUpdateInfoService.updateRiderImage(image,riderId);
    }

    @PostMapping("updateRider")
    public Boolean updateRider(@RequestBody Rider rider){
        return riderUpdateInfoService.updateRider(rider);
    }

    @PostMapping("delRider")
    public Boolean delRider(@RequestBody Integer riderId){
        return riderUpdateInfoService.delRider(riderId);
    }

}
