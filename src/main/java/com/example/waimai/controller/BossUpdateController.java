package com.example.waimai.controller;

import com.example.waimai.pojo.Boss;
import com.example.waimai.service.BossUpdateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
public class BossUpdateController {
    @Autowired
    private BossUpdateService bossUpdateService;

    @PostMapping("updateBoss")
    public Boolean updateBoss(@RequestBody Boss boss){

        return bossUpdateService.updateBoss(boss);
    }

    @PostMapping("updateBossImage")
    public String updateBossImage(@RequestParam("image") MultipartFile image,
                                  @RequestParam("id") String bossId){

        return bossUpdateService.updateBossImage(image,bossId);
    }
}
