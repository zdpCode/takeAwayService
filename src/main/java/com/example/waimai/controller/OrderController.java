package com.example.waimai.controller;

import com.example.waimai.pojo.AliPayBean;
import com.example.waimai.pojo.Orders;
import com.example.waimai.pojo.RiderOrder;
import com.example.waimai.serviceImp.OrdersServiceImp;
import com.example.waimai.util.AliPayUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.data.geo.Point;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class OrderController {


    @Autowired
    private AliPayUtil aliPayUtil;

    @Autowired
    private OrdersServiceImp ordersServiceImp;


    @PostMapping("getOrderInfo")
    public String getOrderInfo(@RequestBody Map<String,String> order){
        // 根据 order 的信息生成 AliPayBean
        AliPayBean aliPayBean = new AliPayBean();
        aliPayBean.setBody("商品描述");
        aliPayBean.setTotal_amount(order.get("totalPrice"));
        aliPayBean.setOut_trade_no(String.valueOf(System.currentTimeMillis())); // 订单号
        aliPayBean.setSubject("订单名称"); // 订单名称
        return aliPayUtil.getOrderInfo(aliPayBean);
    }

    @PostMapping("postOrder")
    public Orders postOrder(@RequestBody Orders orders){
        System.out.println(orders.toString());
        orders = ordersServiceImp.insertNewOrder(orders); // 生成订单
        return orders;
    }

    @PostMapping("getOrdersByRider")
    public List<RiderOrder> getOrdersByRider(@RequestBody Map<String,Object> param){
        // 从 redis 数据库里面查询
        double longitude = (double) param.get("longitude");
        double latitude = (double) param.get("latitude");
        Integer riderId = (Integer) param.get("riderId");
        Point point = new Point(longitude,latitude);
        List<RiderOrder> result = ordersServiceImp.getOrdersByRider(point,riderId);
        System.out.println(result.toString());
        return result;
    }

    @PostMapping("robOrder")
    public Boolean riderRobOrder(@RequestBody Map<String,Integer> param){
        Integer riderId = (Integer) param.get("riderId");
        Integer orderId = (Integer) param.get("orderId");
        return ordersServiceImp.riderRobOrder(riderId,orderId);
    }

    @PostMapping("getOrderByStatus")
    public List<RiderOrder> getOrdersByStatus(@RequestBody Map<String,Integer> param){
        Integer riderId = param.get("riderId");
        Integer riderStatus = param.get("riderStatus");
        return ordersServiceImp.getOrdersByStatus(riderId,riderStatus);
    }

    // 更新订单
    @PostMapping("updateOrderByRider")
    public Boolean updateOrderByRider(@RequestBody Orders orders){

        return ordersServiceImp.updateOrderByRider(orders) == 1;
    }

    // 获取到骑手的所有订单
    @PostMapping("getRiderAllOrder")
    public List<RiderOrder> getRiderAllOrders(@RequestBody Integer RiderId){
        return ordersServiceImp.getRiderAllOrder(RiderId);
    }

    /**
     *
     * @param RiderId 骑手Id
     * @return 返回骑手订单中所有已完成的订单
     */
    @PostMapping("getOrderByRiderIdAndUserStatus")
    public List<Orders> getOrderByRiderIdAndUserStatus(@RequestBody Integer RiderId){

        return ordersServiceImp.getOrderByRiderIdAndUserStatus(RiderId);
    }


    @PostMapping("getOrderByBossIdAndUserStatus")
    public List<Orders> getOrderByBossIdAndUserStatus(@RequestBody Integer bossId){

        return ordersServiceImp.getOrdersByBoosIdAndUserStatus(bossId);
    }

    /**
     * 商家取消订单
     * @param order
     * @return
     */
    @PostMapping("bossCancleOrder")
    public Boolean cancleOrder(@RequestBody Orders order){
        return ordersServiceImp.cancleOrder(order);
    }


}
