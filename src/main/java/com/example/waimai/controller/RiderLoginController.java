package com.example.waimai.controller;

import com.example.waimai.pojo.Rider;
import com.example.waimai.service.RiderLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class RiderLoginController {

    @Autowired
    private RiderLoginService riderLoginService;


    @PostMapping("riderLogin")
    public Rider riderLogin(@RequestBody Map<String,String> param){
        String telephone = param.get("telephone");
        String password = param.get("password");
        return riderLoginService.riderLogin(telephone,password);
    }

    @PostMapping("riderRegister")
    public Rider riderRegister(@RequestBody Rider rider){
        return riderLoginService.riderRegister(rider);
    }

    @PostMapping("getRiderById")
    public Rider getRiderById(@RequestBody Integer id){
        return riderLoginService.getRiderById(id);
    }
}
