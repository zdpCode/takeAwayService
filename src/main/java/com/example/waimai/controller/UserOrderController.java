package com.example.waimai.controller;

import com.example.waimai.pojo.Comment;
import com.example.waimai.pojo.Orders;
import com.example.waimai.pojo.StoreAddress;
import com.example.waimai.pojo.UserAddress;
import com.example.waimai.service.UserOrderService;
import com.example.waimai.serviceImp.UserOrderServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class UserOrderController {

    @Autowired
    private UserOrderServiceImp userOrderService;

    // 用户获取到所有的订单
    @PostMapping("getUserOrderByUserId")
    public List<Orders> getAllOrders(@RequestBody Integer userId){
        return userOrderService.getOrdersByUserId(userId);
    }

    // 更新用户订单信息
    @PostMapping("updateOrderUserStatus")
    public Boolean updateOrderUserStatus(@RequestBody Map<String,Object> param){
        Integer orderId = (Integer) param.get("orderId");
        Integer userStatus = (Integer) param.get("userStatus");
        return userOrderService.updateOrderUsseStatus(orderId,userStatus);
    }

    // 上传评价
    @PostMapping("addComment")
    public Boolean addComment(@RequestBody Comment comment){
        return userOrderService.addComment(comment);
    }


    /**
     *
     * @param order
     * @return 取消订单是否成功
     */
    @PostMapping("userCancleOrder")
    public Boolean userCancleOrder(@RequestBody Orders order){
        return userOrderService.cancleOrder(order);
    }


}
