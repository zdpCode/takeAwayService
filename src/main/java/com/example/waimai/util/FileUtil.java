package com.example.waimai.util;


import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.util.Objects;

/**
 * 文件大小校验工具类

 */
public class FileUtil {

    /**
     * 校验文件大小是否合法 (true-合法, false-不合法)
     *
     * @param file 需要校验的文件
     * @param maxFileSizeStr 单个文件上传的最大值的字符串表示(如10MB)
     * @return 校验文件大小的结果 (true-合法, false-不合法)
     */
    public static boolean fileSizeJudge(MultipartFile file, String maxFileSizeStr) {
        // 先获取配置文件中的设置的单个文件的大小
        if (Objects.isNull(file) || !StringUtils.hasLength(maxFileSizeStr)) {
            throw new IllegalArgumentException("缺少必要的参数");
        }
        /*
         * 转换
         */
        // 文件大小的数值: 如10MB的10, 最终要转换成字节数进行比较
        long num = Long.parseLong(maxFileSizeStr.split("[A-Z]{2,4}")[0]);
        // 文件大小的单位: 如10MB的MB
        String unit = maxFileSizeStr.substring(String.valueOf(num).length()).toUpperCase();
        // 文件大小转为字节
        switch (unit) {
            case "BYTE":
                break;
            case "KB":
                num = num * 1024;
                break;
            case "MB":
                num = num * 1024 * 1024;
                break;
            case "GB":
                num = num * 1024 * 1024 * 1024;
                break;
            case "TB":
                num = num * 1024 * 1024 * 1024 * 1024;
                break;
            default:
                num = 0L;
                break;
        }
        return file.getSize() <= num;
    }

    /**
     * 获取文件的内容类型
     *
     * @param suffix 文件后缀名(包括.)
     * @return 文件的内容类型
     */
    public static String getContentType(String suffix) {
        // 文件的内容类型,默认为纯文本格式(text/plain)
        String contentType;
        switch (suffix.toLowerCase()) {
            case ".bmp":
                contentType = "image/bmp";
                break;
            case ".gif":
                contentType = "image/gif";
                break;
            case ".png":
            case ".jpg":
            case ".jpeg":
                contentType = "image/jpeg";
                break;
            case ".html":
                contentType = "text/html";
                break;
            case ".vsd":
                contentType = "application/vnd.visio";
                break;
            case ".ppt":
            case ".pptx":
                contentType = "application/vnd.ms-powerpoint";
                break;
            case ".doc":
            case ".docx":
                contentType = "application/msword";
                break;
            case ".xml":
                contentType = "text/xml";
                break;
            case ".pdf":
                contentType = "application/pdf";
                break;
            default:
                contentType = "text/plain";
                break;
        }
        return contentType;
    }

}
