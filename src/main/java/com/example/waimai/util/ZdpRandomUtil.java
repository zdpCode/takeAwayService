package com.example.waimai.util;

public class ZdpRandomUtil {
    /*
    * 生成一个n位验证码
    * */
    public static String createRandom(int n){
        StringBuffer s = new StringBuffer();
        for(int i=0;i<n;i++){
           int a =  (int)(Math.random()*10);
           s.append(a);
        }
        //System.out.println(s);
        return s.toString();
    }
}
