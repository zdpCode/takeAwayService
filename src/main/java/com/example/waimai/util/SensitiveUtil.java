package com.example.waimai.util;

import org.springframework.core.io.ClassPathResource;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class SensitiveUtil {
    // 初始化敏感词容器
    public HashMap sensitiveMap;
    // 敏感词集合
    private Set<String> sensitiveWords;

    // 初始化敏感词集合
    public void initSensitiveWords(String fileName)  {
        try{
            ClassPathResource resource = new ClassPathResource("static/"+fileName);
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(resource.getFile()),"UTF-8"));
            sensitiveWords = new HashSet<>();
            String str = null;
            while((str = reader.readLine())!= null){
                sensitiveWords.add(str);
            }
        }catch (IOException e){
            e.printStackTrace();
        }


    }

    // 初始化敏感词容器
    public void initSensitiveMap(){
        // 根据敏感词集合来初始化敏感词容器
        // 该敏感词容器的格式  （大傻子，大傻逼）
        /*
        * sensitiveMap :
        * {'大' ； { "isEnd":0 , '傻' :{"isEnd":1, '子':{"isEnd":1}, '逼': {"isEnd":1} } } }
        * */
        sensitiveMap = new HashMap<>(sensitiveWords.size());
        Map nowMap = null;
        Map<String,String> newMap = null;
        Iterator<String> iterator = sensitiveWords.iterator();
        while(iterator.hasNext()){
            String word = iterator.next();
            nowMap = sensitiveMap;
            for(int i =0;i<word.length();i++){
                char key = word.charAt(i);
                Object wordMap = nowMap.get(key);
                if(wordMap != null){
                    // 说明该字符已经在这个敏感词容器中了，直接使用即可
                    nowMap = (Map)wordMap;
                }else{
                    // 说明该字符还未在敏感词容器中，需要生成新的分支
                    newMap = new HashMap<>();
                    newMap.put("end","0");
                    nowMap.put(key,newMap);
                    nowMap = newMap;
                }
                if(i==word.length() -1){
                    // 到末尾了
                    newMap.put("end","1");
                }

            }
        }
    }

    // 查询敏感词汇
    public String findSensitive(String str){
        char[] strs = str.toCharArray();
        Map nowMap = sensitiveMap;
        int j =0;
        for(int i=0;i<strs.length;i++){
            Map tempMap = (Map) nowMap.get(strs[i]);
            if(tempMap != null){
                nowMap = tempMap; // 深入到以该敏感字符构建的树中
                // 说明含有敏感字母，判断是否为末尾
                if("1".equals(nowMap.get("end"))){
                    // 如果是末尾
                    // 将敏感词替换
                    replace(j,i,strs);
                    nowMap = sensitiveMap; // 开始新的一轮敏感词判断
                }
            }else{
                // 没有敏感字母
                j = i;
                nowMap = sensitiveMap;
            }
        }

        return new String(strs);
    }

    private void replace(int start,int end,char[] strs){
        for(int i = start +1;i<=end;i++){
            strs[i] = '*';
        }
    }
}
