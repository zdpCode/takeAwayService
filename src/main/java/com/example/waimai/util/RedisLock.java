package com.example.waimai.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.data.redis.RedisProperties;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.params.SetParams;

@Component
public class RedisLock {

    @Autowired
    private Jedis jedis;

    // 加锁 不可重入
    public boolean lock(String key,int time){

       return  jedis.set(key, "", SetParams.setParams().nx().ex(time)) != null;
    }

    // 解锁
    public void unlock(String key){
        jedis.del(key);
    }
}
