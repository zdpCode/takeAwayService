package com.example.waimai.util;


import com.sun.mail.util.MailSSLSocketFactory;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.security.GeneralSecurityException;
import java.util.Date;
import java.util.Properties;

public class ZdpMailUtil {

    // 试一下网易的吧
    private static String wyShouquanma = "WGCDPGTIAWFGDTCM";
    private static String myWyEmailAccount = "cect3329@163.com";
    //smtp服务器
    private static String myEmailSMTPHost = "smtp.qq.com";

    /*网易邮箱发送邮件*/
    public static void sendEmailWY(String to,String title, String content) throws MessagingException {
        //1.创建连接对象
        Properties properties = new Properties();
        //1.2 设置发送邮件的服务器
        properties.put("mail.smtp.host","smtp.163.com");
        //1.3 需要经过授权,用户名和密码的校验(必须)
        properties.put("mail.smtp.auth",true);
        //1.4 默认以25端口发送
        properties.setProperty("mail.smtp.socketFactory.class" , "javax.net.ssl.SSLSocketFactory");
        properties.setProperty("mail.smtp.socketFactory.fallback" , "false");
        properties.setProperty("mail.smtp.port" , "465");
        properties.setProperty("mail.smtp.socketFactory.port" , "465");
        //1.5 认证信息
        Session session = Session.getInstance(properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(myWyEmailAccount , wyShouquanma);
            }
        });
        //2.创建邮件对象
        Message message = new MimeMessage(session);
        //2.1设置发件人
        message.setFrom(new InternetAddress(myWyEmailAccount));
        //2.2设置收件人
        message.setRecipient(Message.RecipientType.TO,new InternetAddress(to));
        //2.3设置抄送者(PS:没有这一条网易会认为这是一条垃圾短信，而发不出去)
        message.setRecipient(Message.RecipientType.CC,new InternetAddress(myWyEmailAccount));
        //2.4设置邮件的主题
        message.setSubject(title);
        //2.5设置邮件的内容
        message.setContent(""+content+"","text/html;charset=utf-8");
        //3.发送邮件
        Transport.send(message);
    }


    //发件人
    private static String myEmailAccount = "994401187@qq.com";
    //授权码
    private static String myEmailPassword = "iybjequcyihjbeia";
    /*
    * 发送邮件
    * */
    public static void sendMessage(String toEmailAddress,String emailTitle,String emailContent) throws GeneralSecurityException, MessagingException {
        //存放配置信息
        Properties prop = new Properties();
        prop.setProperty("mail.host",myEmailSMTPHost);///设置QQ邮件服务器
        prop.setProperty("mail.transport.protocol","smtp");///邮件发送协议
        prop.setProperty("mail.smtp.auth","true");//需要验证用户密码
        prop.put("mail.smtp.port",465);
        // 开启debug调试
        prop.setProperty("mail.debug", "true");
        /**SSL认证，注意腾讯邮箱是基于SSL加密的，所以需要开启才可以使用**/
        MailSSLSocketFactory sf = new MailSSLSocketFactory();
        sf.setTrustAllHosts(true);
        //设置是否使用ssl安全连接（一般都使用）
        prop.put("mail.smtp.ssl.enable", "true");
        prop.put("mail.smtp.ssl.socketFactory", sf);
        // 创建session对象
        Session session = Session.getInstance(prop,null);
        //获取信封
        Message msg = new MimeMessage(session);
        //写入内容
        msg.setSubject(emailTitle);
        msg.setContent(emailContent,"text/html;charset=utf-8");
        msg.setSentDate(new Date());
        msg.setFrom(new InternetAddress(myEmailAccount));
        msg.setRecipients(MimeMessage.RecipientType.TO, InternetAddress.parse(toEmailAddress, false));
        //获取到邮差
        Transport transport = session.getTransport();
        //连接邮件服务器
        transport.connect(myEmailSMTPHost,myEmailAccount,myEmailPassword);
        //发送邮件
        transport.sendMessage(msg,msg.getAllRecipients());
    }
}
