package com.example.waimai.serviceImp;

import com.example.waimai.mapper.RiderMapper;
import com.example.waimai.pojo.Rider;
import com.example.waimai.service.RiderLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
@Service
public class RiderLoginServiceImp implements RiderLoginService {

    @Autowired
    private RiderMapper riderMapper;

    @Override
    public Rider riderLogin(String telephone, String password) {
        Rider rider = riderMapper.getRdierByTelAndPwd(telephone,password);
        return rider;
    }

    @Override
    public Rider riderRegister(Rider rider) {
        riderMapper.insertRider(rider);
        return rider;
    }

    @Override
    public Rider getRiderById(Integer id) {
        return riderMapper.getRiderById(id);
    }
}
