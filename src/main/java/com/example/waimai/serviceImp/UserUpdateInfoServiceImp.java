package com.example.waimai.serviceImp;

import com.example.waimai.mapper.UserMapper;
import com.example.waimai.pojo.Rider;
import com.example.waimai.pojo.User;
import com.example.waimai.service.UserUpdateInfoService;
import com.example.waimai.util.OssUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class UserUpdateInfoServiceImp implements UserUpdateInfoService {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private OssUtil ossUtil;

    @Override
    public Boolean updateUser(User user) {
        return userMapper.updateUser(user) == 1;
    }

    @Override
    public String updateUserImage(MultipartFile image, String userId) {
        String url = null;
        try {
            url = ossUtil.uploadFile(image, "UserImage-");
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(!"".equals(url)){
            // 进行更新
            User  user = new User();
            user.setId(Integer.parseInt(userId));
            user.setUserImage(url);
            userMapper.updateUser(user);
        }
        return url;
    }
}
