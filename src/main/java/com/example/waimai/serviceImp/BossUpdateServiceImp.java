package com.example.waimai.serviceImp;

import com.example.waimai.mapper.BossMapper;
import com.example.waimai.pojo.Boss;
import com.example.waimai.pojo.Rider;
import com.example.waimai.service.BossUpdateService;
import com.example.waimai.util.OssUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class BossUpdateServiceImp implements BossUpdateService {
    @Autowired
    private BossMapper bossMapper;
    @Autowired
    private OssUtil ossUtil;


    @Override
    public Boolean updateBoss(Boss boss) {
        return bossMapper.updateBoss(boss) == 1;
    }

    @Override
    public String updateBossImage(MultipartFile image, String bossId) {
        String url = null;
        try {
            url = ossUtil.uploadFile(image, "BossImage-");
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(!"".equals(url)){
            // 进行更新
            Boss boss = new Boss();
            boss.setId(Integer.parseInt(bossId));
            boss.setImage(url);
            bossMapper.updateBoss(boss);
        }
        return url;
    }
}
