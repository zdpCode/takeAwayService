package com.example.waimai.serviceImp;

import com.example.waimai.mapper.RiderMapper;
import com.example.waimai.pojo.Rider;
import com.example.waimai.pojo.Store;
import com.example.waimai.service.RiderUpdateInfoService;
import com.example.waimai.util.OssUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class RiderUpdateInfoServiceImp implements RiderUpdateInfoService {

    @Autowired
    private OssUtil ossUtil;

    @Autowired
    private RiderMapper riderMapper;

    @Override
    public String updateRiderImage(MultipartFile image, String riderId) {
        String url = null;
        try {
            url = ossUtil.uploadFile(image, "RiderImage-");
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(!"".equals(url)){
            // 进行更新
            Rider rider = new Rider();
            rider.setId(Integer.parseInt(riderId));
            rider.setImage(url);
            riderMapper.updateRider(rider);
        }
        return url;
    }

    @Override
    public Boolean updateRider(Rider rider) {
        return riderMapper.updateRider(rider) == 1;
    }

    @Override
    public Boolean delRider(Integer riderId) {
        return riderMapper.delRdier(riderId) == 1;
    }
}
