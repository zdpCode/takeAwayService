package com.example.waimai.serviceImp;

import com.example.waimai.util.ZdpMailUtil;
import com.example.waimai.util.ZdpRandomUtil;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import java.security.GeneralSecurityException;
import java.util.concurrent.TimeUnit;

@Component
public class RabbitMqConsumer {

    @Autowired
    private RedisTemplate redisTemplate;

    @RabbitListener(queuesToDeclare =@Queue("verify"))
    public void sendEmail(String message){
        //邮箱发送验证码
        try {
            String random = ZdpRandomUtil.createRandom(4);
            //验证码存储到redis数据库
            redisTemplate.opsForValue().set("verify-"+message,random,60, TimeUnit.SECONDS);

            String content = "您正在进行邮箱验证，验证码为"+random+".\n验证码将在十分钟后失效";
            ZdpMailUtil.sendMessage(message,"ZDP",content);
        } catch (MessagingException | GeneralSecurityException e) {
            e.printStackTrace();
        }
    }


}
