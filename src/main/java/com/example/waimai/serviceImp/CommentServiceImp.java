package com.example.waimai.serviceImp;

import com.example.waimai.mapper.CommentMapper;
import com.example.waimai.pojo.Comment;
import com.example.waimai.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CommentServiceImp implements CommentService {

    @Autowired
    private CommentMapper commentMapper;

    @Override
    public Comment getCommentByOrderId(Integer orderId) {
        return commentMapper.getCommentByOrderId(orderId);
    }
}
