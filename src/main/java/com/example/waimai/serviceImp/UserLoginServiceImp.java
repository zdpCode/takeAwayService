package com.example.waimai.serviceImp;

import com.example.waimai.mapper.UserMapper;
import com.example.waimai.pojo.User;
import com.example.waimai.service.UserLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class UserLoginServiceImp implements UserLoginService {
    @Autowired
    private UserMapper userMapper;

    @Autowired
    private RedisTemplate redisTemplate;
    @Override
    public User userLogin(String telephone, String password) {
        User user = userMapper.UserLogin(telephone, password);
        System.out.println(user.toString());
        return user;
    }

    @Override
    public User getUserIdByTelephone(String telephone,String password) {
        return userMapper.UserLogin(telephone,password);
    }

    // 注册啊从做
    @Override
    public User register(User user) {
        // 插入用户
        Integer result = userMapper.insertUser(user);
        return user;
    }

    @Override
    public Boolean verifyCode(String email,String code) {
        String code2 = (String) redisTemplate.opsForValue().get("verify-"+email);
        if(code2 != null && code.equals(code2)){
            return true;
        }else{
            return false;
        }
    }
}
