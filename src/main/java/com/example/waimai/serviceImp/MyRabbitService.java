package com.example.waimai.serviceImp;

import com.example.waimai.pojo.Orders;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MyRabbitService {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    // 用户下订单，发送消息到 Boos客户端中让其消费
    public void sendOrderToBoss(String exchange, String routeKey, Orders order){
        rabbitTemplate.convertAndSend(exchange,routeKey,order);
    }

    // 用户取消订单，发送消息到Boos客户端
    public void sendCancleOrderToBoss(String exchange,String routeKey,Orders order){
        rabbitTemplate.convertAndSend(exchange,routeKey,order);
    }

    public void sendOrder(String exchange,String routeKey,Orders order){
        rabbitTemplate.convertAndSend(exchange,routeKey,order);
    }

}
