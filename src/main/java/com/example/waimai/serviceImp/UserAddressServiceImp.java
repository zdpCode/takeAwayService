package com.example.waimai.serviceImp;

import com.example.waimai.mapper.UserAddressMapper;
import com.example.waimai.pojo.UserAddress;
import com.example.waimai.service.UserAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserAddressServiceImp implements UserAddressService {
    @Autowired
    private UserAddressMapper userAddressMapper;
    @Autowired
    private RedisTemplate redisTemplate;

    @Override
    public UserAddress insertIntoUserAddress(UserAddress userAddress) {
        int result = userAddressMapper.insertIntoUserAddress(userAddress);
        System.out.println(userAddress.getId());
        return userAddress;
    }

    @Override
    public List<UserAddress> getAllByUserId(Integer id) {
        List<UserAddress> allByUserId = userAddressMapper.getAllByUserId(id);
        return allByUserId;
    }

    @Override
    public UserAddress getAddressByUserAddressId(Integer id) {
        return userAddressMapper.getAddressByUserAddressId(id);
    }

    @Override
    public Boolean updateUserAddress(UserAddress userAddress) {
        return userAddressMapper.updateUserAddress(userAddress) == 1;
    }
}
