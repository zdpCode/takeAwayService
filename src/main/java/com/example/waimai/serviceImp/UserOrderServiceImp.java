package com.example.waimai.serviceImp;

import com.example.waimai.mapper.*;
import com.example.waimai.pojo.*;
import com.example.waimai.service.UserOrderService;
import com.example.waimai.util.SensitiveUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserOrderServiceImp implements UserOrderService {
    @Autowired
    RedisTemplate redisTemplate;

    @Autowired
    private RiderMapper riderMapper;

    @Autowired
    private BossMapper bossMapper;

    @Autowired
    private OrdersMapper ordersMapper;

    @Autowired
    private StoreMapper storeMapper;
    @Autowired
    private StoreAddressMapper storeAddressMapper;
    @Autowired
    private UserAddressMapper userAddressMapper;

    @Autowired
    private CommentMapper commentMapper;

    @Autowired
    private SensitiveUtil sensitiveUtil;

    @Autowired
    private MyRabbitService myRabbitService;

    @Override
    public List<Orders> getOrdersByUserId(Integer id) {
        List<Orders> result = null;
        result = ordersMapper.getOrdersByUserId(id);
        for(Orders order : result){
            // 获取店铺
            Store store = (Store) redisTemplate.opsForValue().get("Store:"+order.getStoreId());
            if(store == null){
                store = storeMapper.getStore(order.getStoreId());
                redisTemplate.opsForValue().set("Store:"+store.getId(),store);
            }
            // 获取店铺地址
            StoreAddress storeAddress = (StoreAddress) redisTemplate.opsForValue().get("StoreAddr:"+order.getStoreId());
            if(storeAddress == null){
                storeAddress = storeAddressMapper.getStoreAddressById(order.getStoreId());
                redisTemplate.opsForValue().set("StoreAddr:"+order.getStoreId(),storeAddress);
            }
            // 获取用户的地址信息
            UserAddress userAddress = (UserAddress) redisTemplate.opsForValue().get("userAddr:order:"+order.getId());
            if(userAddress == null){
                userAddress = userAddressMapper.getAddressByUserAddressId(order.getUserAddressId());
                redisTemplate.opsForValue().set("userAddr:order:"+order.getId(),userAddress);
            }
            order.setStore(store);
            order.setStoreAddress(storeAddress);
            order.setUserAddress(userAddress);
        }
        return result;
    }

    @Override
    public Boolean updateOrderUsseStatus(Integer ordrrId, Integer userStatus) {
        Boolean result = true;
        Orders orders = ordersMapper.getOrderById(ordrrId);
        if(userStatus == 2){
            // 确认送达 店家状态也要改变
            orders.setStoreStatus(2);
            // 更新骑手的余额 和 商家的 余额
            Rider rider = new Rider();
            rider.setId(orders.getRiderId());
            rider.setMoney(orders.getDeliveryFee());
            result = riderMapper.updateRider(rider) == 1;
            // 找到对应的商家ID
            Boss boss = new Boss();
            boss.setId(storeMapper.getBossIdByStoreId(orders.getStoreId()));
            boss.setMoney(orders.getTotalPrice());
            // 更新商家余额
            result = bossMapper.updateBoss(boss) == 1;
        }
        orders.setUserStatus(userStatus);
        result = ordersMapper.updateOrder(orders) == 1;
        return result;
    }

    // 上传用户评价
    @Override
    public Boolean addComment(Comment comment) {
        // 先进行敏感词过滤
        String riderContent = sensitiveUtil.findSensitive(comment.getRiderContent());
        String storeContent = sensitiveUtil.findSensitive(comment.getStoreContent());
        comment.setRiderContent(riderContent);
        comment.setStoreContent(storeContent);
        Integer integer = commentMapper.insertComment(comment);
        // 更改一下订单的状态
        Orders orders = new Orders();
        orders.setId(comment.getOrderId());
        orders.setUserStatus(3);
        ordersMapper.updateOrder(orders);
        return integer == 1;
    }

    @Override
    public Boolean cancleOrder(Orders order) {
        int result = ordersMapper.updateOrder(order);
        if(result == 1){
            // 更新成功
            // rabbitMq 通知 店铺
            String exchange = "Bossorders";
            String routeKey ="store:"+order.getStoreId();
            myRabbitService.sendCancleOrderToBoss(exchange,routeKey,order);
        }
        return result == 1;
    }
}
