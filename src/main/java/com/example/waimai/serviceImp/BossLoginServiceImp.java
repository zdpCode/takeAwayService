package com.example.waimai.serviceImp;

import com.example.waimai.mapper.BossMapper;
import com.example.waimai.pojo.Boss;
import com.example.waimai.service.BossLoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BossLoginServiceImp implements BossLoginService {

    @Autowired
    private BossMapper bossMapper;

    @Override
    public Boss bossLogin(String telephone, String password) {
        Boss boss = bossMapper.selectBytelAndPwd(telephone, password);
        return boss;
    }

    @Override
    public Boss bossRegister(Boss boss) {
        bossMapper.insertBoss(boss);
        return boss;
    }

    @Override
    public Boss getBossById(Integer id) {
        return bossMapper.selectBossById(id);
    }
}
