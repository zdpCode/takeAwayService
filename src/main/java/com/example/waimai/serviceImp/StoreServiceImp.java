package com.example.waimai.serviceImp;

import com.example.waimai.mapper.FoodMapper;
import com.example.waimai.mapper.FoodTypeMapper;
import com.example.waimai.mapper.StoreMapper;
import com.example.waimai.pojo.Food;
import com.example.waimai.pojo.FoodType;
import com.example.waimai.pojo.RiderOrder;
import com.example.waimai.pojo.Store;
import com.example.waimai.service.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.*;
import org.springframework.data.redis.connection.RedisGeoCommands;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class StoreServiceImp implements StoreService {

    @Autowired
    private StoreMapper storeMapper;
    @Autowired
    private FoodMapper foodMapper;
    @Autowired
    private FoodTypeMapper foodTypeMapper;

    @Autowired
    private RedisTemplate redisTemplate;


    @Override
    public List<Store> getAllStore() {
        List<Store> stores = storeMapper.getStores();
        String key = "Store:";
        for(Store s : stores){
            redisTemplate.opsForValue().set(key+s.getId(),s);
        }
        return stores;
    }

    @Override
    public List<FoodType> loadStoreHome(Integer id) {
        List<FoodType> foodTypes = foodTypeMapper.getFoodTypesByStoreId(id);
        List<Food> foods = foodMapper.getFoodsByStoreId(id);
        if(foods.size() != 0 && foodTypes.size() != 0){
            // 开始插入
            int foodTypeIndex = 0;
            int foodIndex = 0;
            FoodType temp = foodTypes.get(foodTypeIndex);
            while(foodIndex < foods.size()){
                Food foodTemp = foods.get(foodIndex);
                if(foodTemp.getFoodTypeId() == temp.getId()){
                    foodTemp.setFoodTypeName(temp.getFoodTypeName());
                    temp.getSubFoods().add(foodTemp);
                    foodIndex ++;
                }else if(foodTemp.getFoodTypeId() > temp.getId()){ // 当前菜品的FoodTypeId 大于 菜品类型Id 菜品类型需要更新
                    temp = foodTypes.get(++foodTypeIndex);
                }else{
                    // 菜所属的id 比当前菜品类型的最小还小
                    break;
                }
            }
        }
        // 菜品信息是会修改的，存进去干嘛
        redisTemplate.opsForValue().set("StoreFoodList:"+id,foods);
        return foodTypes;
    }


    @Override
    public List<Food> getFoodsByStoreId(Integer id) {
        List<Food> foods = (List<Food>) redisTemplate.opsForValue().get("StoreFoodList:"+id);
        if(foods != null){
            // 删除一下
            redisTemplate.delete("StoreFoodList:"+id);
            return foods;
        }else{
            // 处理一下返回
            return foodMapper.getFoodsByStoreId(id);
        }
    }

    @Override
    public Store getStore(Integer id) {
        Store store = storeMapper.getStore(id);
        if(store!= null){
            redisTemplate.opsForValue().set("Store:"+store.getId(),store);
        }
        return store;
    }

    /*
    * 获取附近的店铺
    * */

    @Override
    public List<Store> getMissedStore(String longitude, String latitude) {
        // 查询5km内的店铺
        Distance distance = new Distance(5, Metrics.KILOMETERS);
        Point point = new Point(Double.parseDouble(longitude),Double.parseDouble(latitude));
        // 创建一个圆形范围
        Circle circle = new Circle(point,distance);
        GeoResults radius = redisTemplate.opsForGeo().radius("missedStore",circle); // 查询附近的店铺
        List<GeoResult> content = radius.getContent(); // 附近的店铺
        List<Store> result = new ArrayList<Store>();
        for(GeoResult c :content){
            RedisGeoCommands.GeoLocation<String> s = (RedisGeoCommands.GeoLocation) c.getContent();
            String ss = s.getName();
            String sss = ss.split(":")[1];
            Integer storeId = Integer.parseInt(sss);
            Store store = storeMapper.getStore(storeId);
            result.add(store);
        }
        return result;
    }
    /*// 测试方法
    public HashMap<String,Object> getFoodMap(Integer id){
        List<FoodType> foodTypes = foodTypeMapper.getFoodTypesByStoreId(id);
        List<Food> foods = foodMapper.getFoodsByStoreId(id);
        // 开始插入
        int foodTypeIndex = 0;
        int foodIndex = 0;
        FoodType temp = foodTypes.get(foodTypeIndex);
        while(foodIndex < foods.size()){
            Food foodTemp = foods.get(foodIndex);
            if(foodTemp.getFoodTypeId() == temp.getId()){
                foodTemp.setFoodTypeName(temp.getFoodTypeName());
                temp.getSubFoods().add(foodTemp);
                foodIndex ++;
            }else if(foodTemp.getFoodTypeId() > temp.getId()){
                temp = foodTypes.get(++foodIndex);
            }else{
                // 菜所属的id 比当前菜品类型的最小还小
                break;
            }
        }
        HashMap<String,Object> result = new HashMap<>();
        result.put("foods",foods);
        result.put("foodTypes",foodTypes);
        return result;
    }*/
}
