package com.example.waimai.serviceImp;

import com.example.waimai.mapper.StoreAddressMapper;
import com.example.waimai.pojo.StoreAddress;
import com.example.waimai.service.StoreAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class StoreAddressServiceImp implements StoreAddressService {

    @Autowired
    private StoreAddressMapper storeAddressMapper;
    @Autowired
    private RedisTemplate redisTemplate;
    String key = "StoreAddr:";

    @Override
    public StoreAddress getStoreAddressById(Integer storeid) {
        StoreAddress storeAddr = storeAddressMapper.getStoreAddressById(storeid);
        redisTemplate.opsForValue().set(key+storeid,storeAddr);
        return storeAddr;
    }
}
