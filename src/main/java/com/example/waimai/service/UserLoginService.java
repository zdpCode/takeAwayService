package com.example.waimai.service;

import com.example.waimai.pojo.User;

public interface UserLoginService {

    public User userLogin(String telephone,String password);

    public User getUserIdByTelephone(String telephone, String password);

    // 注册
    User register(User user);

    // 验证验证码
    Boolean verifyCode(String email,String code);
}
