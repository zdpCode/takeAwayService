package com.example.waimai.service;

import com.example.waimai.pojo.Rider;
import org.springframework.web.multipart.MultipartFile;

public interface RiderUpdateInfoService {

    String updateRiderImage( MultipartFile image,String riderId);

    Boolean updateRider(Rider rider);

    Boolean delRider(Integer riderId);
}
