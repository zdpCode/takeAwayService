package com.example.waimai.service;

import com.example.waimai.pojo.Boss;
import com.example.waimai.pojo.User;
import org.springframework.web.multipart.MultipartFile;

public interface UserUpdateInfoService {
    Boolean updateUser(User user);

    String updateUserImage(MultipartFile image,String userId);
}
