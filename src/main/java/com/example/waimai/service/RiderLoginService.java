package com.example.waimai.service;

import com.example.waimai.pojo.Rider;

import java.util.Map;

public interface RiderLoginService {

     Rider riderLogin(String telephone, String password);

     Rider riderRegister(Rider rider);

     Rider getRiderById(Integer id);
}
