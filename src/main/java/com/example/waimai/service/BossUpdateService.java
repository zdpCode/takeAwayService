package com.example.waimai.service;

import com.example.waimai.pojo.Boss;
import org.springframework.web.multipart.MultipartFile;

public interface BossUpdateService {

    public Boolean updateBoss(Boss boss);

    String updateBossImage(MultipartFile image, String bossId);
}
