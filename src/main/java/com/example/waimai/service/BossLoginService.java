package com.example.waimai.service;

import com.example.waimai.pojo.Boss;

public interface BossLoginService {

    Boss bossLogin(String telephone,String password);

    Boss bossRegister(Boss boss);

    Boss getBossById(Integer id);
}
