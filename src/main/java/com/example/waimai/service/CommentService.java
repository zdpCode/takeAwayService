package com.example.waimai.service;

import com.example.waimai.pojo.Comment;

public interface CommentService {

    Comment getCommentByOrderId(Integer orderId);
}
