package com.example.waimai.service;

import com.example.waimai.pojo.Comment;
import com.example.waimai.pojo.Orders;
import com.example.waimai.pojo.StoreAddress;
import com.example.waimai.pojo.UserAddress;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public interface UserOrderService {

    List<Orders> getOrdersByUserId(Integer id);

    Boolean updateOrderUsseStatus(Integer ordrrId,Integer userStatus);

    // 上传评价
    Boolean addComment(Comment comment);

    // 用户取消订单
    Boolean cancleOrder(Orders order);


}
