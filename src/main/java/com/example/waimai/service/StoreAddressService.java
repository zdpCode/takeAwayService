package com.example.waimai.service;

import com.example.waimai.pojo.StoreAddress;

public interface StoreAddressService {
    // 根据 Storeid 找 StoreAddress
    StoreAddress getStoreAddressById(Integer storeid);
}
