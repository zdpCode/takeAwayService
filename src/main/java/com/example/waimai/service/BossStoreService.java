package com.example.waimai.service;

import com.example.waimai.pojo.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface BossStoreService {
    FoodType insertFoodType(FoodType foodType);

    // 商家删除对应菜品
    Boolean deleteFood(Food food);

    // 更新菜品
    Boolean updateFood(Food food);

    // 更新店铺图片
    String updateStoreImage(MultipartFile image,String id);

    // 更新菜品图片
    String updateFoodImage(MultipartFile image,String id);

    // 获取到图片的URL地址
    String getImageUrl(MultipartFile image);

    // 添加菜品
    Integer addFood(Food food);

    // 删除店铺
    Boolean delStore(Integer storeId);
    // 获取所有店铺
    List<Store> getAllStore(Integer bossId);

    // 获取指定店铺的地址
    StoreAddress getStoreAddress(Integer storeId);

    // 获取指定状态的订单
    List<Orders> getOrdersByStoreStatus(Integer storeId, Integer storeStatus);

    // 商家接收订单
    Boolean receiveOrder(Integer orderId);

    // 根据菜品iD获取到菜品
    Food getFood(Integer foodId);

    // 上传店铺地址
    Boolean addStoreAddr(StoreAddress storeAddress);

    // 创建店铺
    Store addStore(Store store);

    // 删除店铺 彻底删除
    Boolean realDelStore(Integer storeId);

    // 更新店铺地址
    Boolean updateStoreAddr(StoreAddress storeAddress);

    // 更新店铺信息
    Boolean updateStore(Store store);
}
