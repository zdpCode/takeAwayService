package com.example.waimai.service;

import com.example.waimai.pojo.UserAddress;

import java.util.List;

public interface UserAddressService {

    public UserAddress insertIntoUserAddress(UserAddress userAddress);

    public List<UserAddress> getAllByUserId(Integer id);

    public UserAddress getAddressByUserAddressId(Integer id);

    Boolean updateUserAddress(UserAddress userAddress);
}
