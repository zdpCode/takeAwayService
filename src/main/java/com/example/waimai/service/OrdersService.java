package com.example.waimai.service;

import com.example.waimai.pojo.Orders;
import com.example.waimai.pojo.RiderOrder;
import org.springframework.data.geo.Point;

import java.util.List;

public interface OrdersService {

    Orders insertNewOrder(Orders order);

    List<RiderOrder> getOrdersByRider(Point point, Integer riderId);

    // 骑手抢单
    Boolean riderRobOrder(Integer riderId ,Integer orderId);

    List<RiderOrder> getOrdersByStatus(Integer riderId,Integer riderStatus);

    Integer updateOrderByRider(Orders order);

    // 获取到骑手的全部订单
    List<RiderOrder> getRiderAllOrder(Integer riderId);

    List<Orders> getOrdersByBoosIdAndUserStatus(Integer bossId);

    List<Orders> getOrderByRiderIdAndUserStatus(Integer RiderId);

    Boolean cancleOrder(Orders order);
}
