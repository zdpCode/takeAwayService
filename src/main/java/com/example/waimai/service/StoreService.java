package com.example.waimai.service;

import com.example.waimai.pojo.Food;
import com.example.waimai.pojo.FoodType;
import com.example.waimai.pojo.Store;

import java.util.List;
import java.util.Map;

public interface StoreService {
    // 用户主界面
    public List<Store> getAllStore();
    // 获取到菜品的详细信息 和 菜品类型
    public List<FoodType> loadStoreHome(Integer id);
    // 获取菜品的集合
    public List<Food> getFoodsByStoreId(Integer id);

    public Store getStore(Integer id);
    // 获取附近的店铺
    List<Store> getMissedStore(String longitude,String latitude);
}
